package com.cgi.dentistapp.dao.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "dentist_visit")
public class DentistVisitEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "visit_time")
    private Date visitTime;

    @Column(name = "doctor_name")
    private String dentistName;

    public DentistVisitEntity() {
    }

    public DentistVisitEntity(String dentistName, Date visitTime) {
        this.setVisitTime(visitTime);
        this.setDentistName(dentistName);

    }

    public DentistVisitEntity(Long id, String dentistName, Date visitTime) {
        this.setId(id);
        this.setVisitTime(visitTime);
        this.setDentistName(dentistName);

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getVisitTime() { return visitTime; }

    public void setVisitTime(Date visitTime) {
        this.visitTime = visitTime;
    }

    public String getDentistName() { return dentistName; }

    public void setDentistName(String dentistName) { this.dentistName = dentistName; }


}
